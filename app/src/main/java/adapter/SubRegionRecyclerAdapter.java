package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.Callback;

import butterknife.BindView;
import butterknife.ButterKnife;
import examplesoureit.countryretrofitsourceithomework.CountryActivity;
import examplesoureit.countryretrofitsourceithomework.R;
import examplesoureit.countryretrofitsourceithomework.RegionRetrofit;
import examplesoureit.countryretrofitsourceithomework.SubRegionActivity;
import model.SubRegion;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubRegionRecyclerAdapter extends RecyclerView.Adapter<SubRegionRecyclerAdapter.ViewHolder> {

    public static final String MY_TAG = "country name";

    private List<SubRegion> list = new ArrayList<>();
    private Context context;

    public SubRegionRecyclerAdapter(List<SubRegion> list) {
        this.list = list;
    }

    public SubRegionRecyclerAdapter(List<SubRegion> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @NonNull
    @Override
    public SubRegionRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.region_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubRegionRecyclerAdapter.ViewHolder holder, final int position) {
        holder.regionText.setText(list.get(position).getSubReregion().toString());
//


        holder.regionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, list.get(position).getSubReregion().toString(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(), CountryActivity.class);
                intent.putExtra(MY_TAG, list.get(position).getSubReregion().toString());
                v.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.region_item_text)
        TextView regionText;

//        @BindView(R.id.subregion_relative_layout)   //Не могу понять, почему при раскомментировании этого куска кода появляется ошибка
//        RelativeLayout relativeLayout;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
