package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import examplesoureit.countryretrofitsourceithomework.MainActivity;
import examplesoureit.countryretrofitsourceithomework.R;
import examplesoureit.countryretrofitsourceithomework.SubRegionActivity;
import model.Region;

public class RegionRecyclerAdapter  extends RecyclerView.Adapter<RegionRecyclerAdapter.ViewHolder>{

    private List<Region> list;
    private Context context;

    private static final String SUB_REGION = "sub_region";

    public RegionRecyclerAdapter(List<Region> list, Context context) {
        this.list = list;
        this.context = context;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.region_listitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.regionText.setText(list.get(position).getName().toString());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), SubRegionActivity.class);
                intent.putExtra(SUB_REGION, list.get(position).getSubName());
                v.getContext().startActivity(intent);
            }
        });

        holder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, list.get(position).getSubName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.region_item_text)
        TextView regionText;

        @BindView(R.id.region_item_relative_layout)
        RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
