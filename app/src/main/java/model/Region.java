package model;

public class Region {
    private String name;
    private String subName;

    public Region(String name, String subName) {
        this.name = name;
        this.subName = subName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }
}
