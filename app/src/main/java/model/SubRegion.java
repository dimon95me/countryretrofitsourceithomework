package model;

import com.google.gson.annotations.SerializedName;

public class SubRegion {

    public SubRegion(String subRegion) {
        this.subReregion = subRegion;
    }

    @SerializedName("name")
    public String subReregion;

    public String getSubReregion() {
        return subReregion;
    }

    public void setSubReregion(String subReregion) {
        this.subReregion = subReregion;
    }
}
