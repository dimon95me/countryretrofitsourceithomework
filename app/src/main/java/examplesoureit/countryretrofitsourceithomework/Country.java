package examplesoureit.countryretrofitsourceithomework;

import com.google.gson.annotations.SerializedName;

public class Country {

    @SerializedName("name")
    private String name;
    @SerializedName("population")
    private String population;
    @SerializedName("capital")
    private String capital;
    @SerializedName("area")
    private String area;
    @SerializedName("numericCode")
    private String numericCode;

    public Country(String name, String population, String capital, String area, String numericCode) {
        this.name = name;
        this.population = population;
        this.capital = capital;
        this.area = area;
        this.numericCode = numericCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }
}
