package examplesoureit.countryretrofitsourceithomework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import adapter.CountryRecyclerAdapter;
import adapter.SubRegionRecyclerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CountryActivity extends AppCompatActivity {

    @BindView(R.id.country_name)
    TextView name;

    @BindView(R.id.country_population)
    TextView population;
    @BindView(R.id.country_capital)
    TextView capital;
    @BindView(R.id.country_area)
    TextView area;
    @BindView(R.id.country_numeric_code)
    TextView numericCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        ButterKnife.bind(this);

        String value = null;

        if (getIntent().getStringExtra(CountryRecyclerAdapter.COUNTRY)!=null){
            value = getIntent().getStringExtra(CountryRecyclerAdapter.COUNTRY);
        }

        if (getIntent().getStringExtra(SubRegionRecyclerAdapter.MY_TAG)!=null){
            value = getIntent().getStringExtra(SubRegionRecyclerAdapter.MY_TAG);
        }

        RegionRetrofit.getCountry(value, new Callback<List<Country>>() {
            @Override
            public void success(List<Country> countries, Response response) {
           Country country = countries.get(0);
           name.setText("Name: "+country.getName().toString());
           population.setText("Population: "+country.getPopulation().toString());
           area.setText("Area: "+country.getArea().toString());
           capital.setText("Capital: "+country.getCapital().toString());
           numericCode.setText("Numeric code: "+country.getNumericCode().toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "You are have a trouble: "+error.getKind(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
