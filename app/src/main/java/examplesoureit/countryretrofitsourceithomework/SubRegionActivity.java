package examplesoureit.countryretrofitsourceithomework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.RegionRecyclerAdapter;
import adapter.SubRegionRecyclerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.SubRegion;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SubRegionActivity extends AppCompatActivity {

    @BindView(R.id.sub_region_recycler)
    RecyclerView subRegionRecycler;

    List<SubRegion> subRegionList = new ArrayList<>();
    SubRegionRecyclerAdapter subRegionRecyclerAdapter;

    String regionName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_region);
        ButterKnife.bind(this);

        Toast.makeText(this, getIntent().getStringExtra("sub_region"), Toast.LENGTH_SHORT).show();

        regionName = getIntent().getStringExtra("sub_region");

        RegionRetrofit.getRegions(regionName.toLowerCase(), new Callback<List<SubRegion>>() {
            @Override
            public void success(List<SubRegion> regions, Response response) {
                subRegionList = regions;
                subRegionRecyclerAdapter = new SubRegionRecyclerAdapter(subRegionList, getApplicationContext());
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                subRegionRecycler.setLayoutManager(layoutManager);
                subRegionRecycler.setItemAnimator(new DefaultItemAnimator());
                subRegionRecycler.setAdapter(subRegionRecyclerAdapter);

                subRegionRecyclerAdapter.notifyDataSetChanged();
                Log.d("my_tag", "regions: "+String.valueOf(regions.size())+" retrofit");
                Log.d("my_tag", "regionList: "+String.valueOf(regions.size())+" retrofit");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("my_tag", "You are have error" + error.getKind());
                Toast.makeText(SubRegionActivity.this, "You are have error" + error.getKind(), Toast.LENGTH_SHORT).show();
            }
        });

        subRegionRecyclerAdapter = new SubRegionRecyclerAdapter(subRegionList, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        subRegionRecycler.setLayoutManager(layoutManager);
        subRegionRecycler.setItemAnimator(new DefaultItemAnimator());
        subRegionRecycler.setAdapter(subRegionRecyclerAdapter);

        subRegionRecyclerAdapter.notifyDataSetChanged();


        Log.d("my_tag", String.valueOf(subRegionList.size())+" main");


    }
}
