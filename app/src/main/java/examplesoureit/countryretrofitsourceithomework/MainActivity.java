package examplesoureit.countryretrofitsourceithomework;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.CountryRecyclerAdapter;
import adapter.RegionRecyclerAdapter;
import adapter.SubRegionRecyclerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import model.Region;
import model.SubRegion;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.region_recycler)
    RecyclerView regionRecycler;

    private List<Region> regionList = new ArrayList<>();
    RegionRecyclerAdapter regionRecyclerAdapter;

    Dialog dialog;

    @BindView(R.id.maaain)
    LinearLayout maaain;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        generateRegions();

        regionRecyclerAdapter = new RegionRecyclerAdapter(regionList, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        regionRecycler.setLayoutManager(layoutManager);
        regionRecycler.setItemAnimator(new DefaultItemAnimator());
        regionRecycler.setAdapter(regionRecyclerAdapter);

        regionRecyclerAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        regionRecyclerAdapter = new RegionRecyclerAdapter(regionList, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        regionRecycler.setLayoutManager(layoutManager);
        regionRecycler.setItemAnimator(new DefaultItemAnimator());
        regionRecycler.setAdapter(regionRecyclerAdapter);

        regionRecyclerAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == item.getItemId()){
            searchingDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void searchingDialog(){

        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        View dialogView = inflater.inflate(R.layout.dialog_item, null);
        final EditText itemSearchText = dialogView.findViewById(R.id.dialog_request_value);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("What city you search?");
        builder.setView(dialogView);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RegionRetrofit.getCountry(itemSearchText.getText().toString(), new Callback<List<Country>>() {
                    @Override
                    public void success(List<Country> countries, Response response) {
//                        Country country = countries.get(0);
                        CountryRecyclerAdapter countryRecyclerAdapter = new CountryRecyclerAdapter(countries, getApplicationContext());
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        regionRecycler.setLayoutManager(layoutManager);
                        regionRecycler.setItemAnimator(new DefaultItemAnimator());
                        regionRecycler.setAdapter(countryRecyclerAdapter);

                        countryRecyclerAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), "You are have a trouble: "+error.getKind(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();


//        dialog = new Dialog(MainActivity.this);
//        dialog.setContentView(R.layout.dialog_item);
//        dialog.setTitle("Enter country");
//
//        ok = findViewById(R.id.ok);
//        cancel = findViewById(R.id.cancel);
//        questionText = findViewById(R.id.request_value);
//
//        dialog.show();
//
//
//        ok.setEnabled(true);
//        cancel.setEnabled(true);
//
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                RegionRetrofit.getRegions("alb", new Callback<List<SubRegion>>() {
//                    @Override
//                    public void success(List<SubRegion> subRegions, Response response) {
//                        SubRegionRecyclerAdapter adapter = new SubRegionRecyclerAdapter(subRegions, getApplicationContext());
//                        regionRecycler.setAdapter(adapter);
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        Toast.makeText(getApplicationContext(), "You have a problem: " + error.getKind().toString(), Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        });


    }

    private void generateRegions(){
        regionList.add(new Region("European Union", "EU" ));
        regionList.add(new Region("European Free Trade Association", "EFTA" ));
        regionList.add(new Region("Caribbean Community", "CARICOM" ));
        regionList.add(new Region("Pacific Alliance", "PA" ));
        regionList.add(new Region("African Union", "AU" ));
        regionList.add(new Region("Union of South American Nations", "USAN" ));
        regionList.add(new Region("Eurasian Economic Union", "EEU" ));
        regionList.add(new Region("Arab League", "AL" ));
        regionList.add(new Region("Association of Southeast Asian Nations", "ASEAN" ));
        regionList.add(new Region("Central American Integration System", "CAIS" ));
        regionList.add(new Region("Central European Free Trade Agreement", "CEFTA" ));
        regionList.add(new Region("North American Free Trade Agreement", "NAFTA" ));
        regionList.add(new Region("South Asian Association for Regional Cooperation", "SAARC" ));
    }


}

