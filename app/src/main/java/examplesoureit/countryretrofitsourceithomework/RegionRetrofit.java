package examplesoureit.countryretrofitsourceithomework;

import java.util.List;

import model.Region;
import model.SubRegion;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

public class RegionRetrofit {
    private static final String ENDPOINT = "http://restcountries.eu/rest/v2";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    private static void initialize(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    interface ApiInterface{
        @GET("/regionalbloc/{region}")
        void getRegions(@Path("region") String region, Callback<List<SubRegion>> callback);

        @GET("/name/{name}")
        void getCountry(@Path("name") String countryName, Callback<List<Country>> callback);

    }

    public static void getRegions(String region, Callback<List<SubRegion>> callback){
        apiInterface.getRegions(region, callback);
    }

    public static void getCountry(String countryName, Callback<List<Country>> callback){
        apiInterface.getCountry(countryName, callback);
    }
}
